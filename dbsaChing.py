import pyodbc 
# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
server = '.' 
database = 'iching' 
username = 'sa' 
password = 'iChing2021!' 
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

#Sample select query
cursor.execute("SELECT @@version;") 
row = cursor.fetchone() 
while row: 
    print(row[0])
    row = cursor.fetchone()

cursor = cnxn.cursor()

cursor.execute('''

                INSERT INTO iching.dbo.People (Name, Age, City)
                VALUES
                ('Jade',20,'London'),
                ('Mary',47,'Boston'),
                ('Jon',35,'Paris')  

                ''')

cnxn.commit()