import pyodbc
import pandas as pd
from datetime import datetime

# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
server = '.' 
database = 'Ching' 
username = 'sa' 
password = 'iChing2021!' 
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cnxn.autocommit = True
cursor = cnxn.cursor()

cursor.execute('USE Ching')

df = pd.read_csv("csv-CHINGTABLE.csv", delimiter=';')

df['HOJE'] = pd.to_datetime(df['HOJE']).dt.strftime('%Y-%m-%d %H:%M:%S')

columns = ['ID', 'HEXAGRAMA', 'EXP', 'HOJE']

df_data = df[columns]

record = df_data.values.tolist()


sql_insert =  '''
     INSERT INTO velhochines
     VALUES (?, ?, ?, ?)
'''

try:
    cursor.executemany(sql_insert, record)
    cursor.commit();    
except Exception as e:
    cursor.rollback()
    print(str(e[1]))
finally:
    print("insert concluido")
    cursor.close()
    cnxn.close()

